import vibe.vibe;

final class Room {
	LocalManualEvent messageEvent;

	string[] messageStore;

	this() {
		messageEvent = createManualEvent();
	}

	void addMessage(string name, string message) {
		messageStore ~= name ~ ": " ~ message;
	}

	void waitForMessage(size_t nextMessage) {
		while (messageStore.length <= nextMessage) {
			messageEvent.wait();
		}
	}
}