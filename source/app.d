import vibe.vibe;
import chatapp;

void main() {
	URLRouter router = new URLRouter;

	router.registerWebInterface(new ChatApp);

	router.get("*", serveStaticFiles("public/"));

	HTTPServerSettings settings = new HTTPServerSettings;
	settings.port = 8080;
	settings.bindAddresses = ["::1", "127.0.0.1"];

	listenHTTP(settings, router);
	logInfo("http://localhost:8080");

	runApplication();
}