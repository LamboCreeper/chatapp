import vibe.vibe;
import room;

final class ChatApp {
	private Room[string] roomStore;

	// Homepage

	void get() {
		render!("index.dt");
	}

	// Rooms 

	void getRoom(string id, string name) {
		string[] messages = getOrCreateRoom(id).messageStore;

		render!("room.dt", id, name, messages);
	}

	void postRoom(string id, string name, string message) {
		if (message.length) {
			getOrCreateRoom(id).addMessage(name, message);
		}	
		redirect("room?id=" ~ id.urlEncode ~ "&name=" ~ name.urlEncode);
	}

	private Room getOrCreateRoom(string id) { // change my name
		if (auto pr = id in roomStore) {
			return *pr;
		}
		return roomStore[id] = new Room;
	}

	// Web Sockets

	void getWS(string room, string name, scope WebSocket socket) {
		Room chatRoom = getOrCreateRoom(room);
		Task writer = runTask({
			size_t nextMessage = chatRoom.messageStore.length;

			while (socket.connected) {
				while (nextMessage < chatRoom.messageStore.length) {
					socket.send(chatRoom.messageStore[nextMessage++]);
				}
				chatRoom.waitForMessage(nextMessage);
			}
		});

		while (socket.waitForData) {
			string message = socket.receiveText();

			if (message.length) {
				chatRoom.addMessage(name, message);
			}
		}

		writer.join();
	}
}