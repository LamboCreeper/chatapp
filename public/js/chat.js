function sendMessage() {
	const message = document.querySelector("#inputLine");

	socket.send(message.value);
	message.value = "";

	return false;
}

function connect(room, name) {
	const socket = new WebSocket(`ws://127.0.0.1:8080/ws?room=${encodeURIComponent(room)}&name=${encodeURIComponent(name)}`);

	socket.onmessage = (message) => {
		const history = document.querySelector("#history");
		const previous = history.innerHTML.trim();

		if (previous.length) {
			previous += "\n";
		}

		history.innerHTML = previous + message.data;
		history.scrollTop = history.scrollHeight;
	}	

	socket.onclose = () => {
		console.log("Reconnecting...");
		connect();
	}
}