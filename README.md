# ChatApp

A basic chat app written in D.

## Known Issues
- New messages don't appear in chat until you send a new message.

## Future Additions
- Store messages in Redis rather than an array.
- Support user accounts.
- Support chat markdown.
